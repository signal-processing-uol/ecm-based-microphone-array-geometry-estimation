%% Script for testing microphone array geometry estimation
% Framework coded by: Klaus Bruemann
% (with the exception of the included .p-files and Habest RIR generator)
% 
% The implementation is largely based on:
% "Blind Microphone Geometry Calibration Using One Reverberant 
% Speech Event", Schwartz et al., 2017
% 
% However, it contains proposed improvements suggested in:
% "Data-Dependent Initialization for ECM-Based Blind Geometry Estimation of
% a Microphone Array Using Reverberant Speech", Bruemann et al. 2021

clear; clc; 
close all;
restoredefaultpath;
addpath(genpath('Auxiliary_Functions'));

%% Main Variables
% Simulation Options: {'Artificial', 'HabetsRIR'}
Variable.Sim_type = 'Artificial';

% Coherence Matrix Initialization Options:
% - Identity matrix
% - sinc (uses oracle information)
% - Phi_x (proposed initial coherence matrix)
% {'Identity','sinc','Phi_x'};
Variable.Gamma_Init_type = 'Phi_x';

% RDTF/RETF Vector g Options: {'TDoA','GCCPHAT','T=0','T=2.5','T=5','T=7.5','T=10','T=12.5','T=15'};
% 'T = 0' is pretty much equivalent to oracle RDTF. Higher values of T [ms]
% rely on the oracle RIR (not possible for Artificial scenario).
Variable.RDTF_type = 'TDoA';

% Vary filter used to estimate the initial coherence matrix:
% - matched beamformer (MVDR)
% - proposed Time-invariant MPDR (MPDR)
% - proposed Time-varying MPDR (MPDR2)
% {'MVDR','MPDR','MPDR2'}
Variable.Init_filter = 'MVDR';
%% Parameter and Method Initialization
% Numerical Adjustments
Param.eps = db2pow(-60); % numerical floor for PSDs (ensure positive-def.)
Param.loading_factor = 0.01; % diagonal loading value

% Hidden/Latent Parameters
Latent.l = 1.5; % cube length [m] (length of cube in which mics can be located)
Latent.scenario_rng = 1; % seed to consistently generate same random scenario each time
Latent.P = [6;6;2.4]; % Room geometry
Latent.RDR = 0; % RDR: Reverberant-to-direct ratio [dB] {-66 : 4}

% Known Scenario Parameters:
Param.N = 6; % N: number of microphones
Param.c = 340; % c: speed [m/s] of sound
Param.l_max = max(0.3, sqrt(3*Latent.l^2)); % maximal 'plausible' distance
% Framework Parameters:
Param.K = 1024; %512; % Frame length
Param.R = Param.K/2; %K/4; % Frame shift
Param.L = 62.5*2^(11-log2(Param.R)); % Defines number of STFT frames, for K=256: L=800.
Param.fs = 48e3; % fs: sampling rate
Param.fr = [0.1e3, 3e3]; % lower and upper limits for frequency range
Param.ECM_max_iter = 3; % number of ECM iterations
Param.Ana_window = 'sqrtHann'; % STFT analysis window

%% Generate Acoustic Scenario:
[Param, Latent] = func_Generate_Scenario_Geometry(Param, Latent);

%% Generate Signal (and relevant parameters)
[Param, Latent, Variable] = func_Generate_STFT_Signal(Param, Latent, Variable);

%% Choose RDTF Initialization: g(k)
Param = func_Choose_g_init(Param, Latent, Variable);

%% Initialize Estimated Coherence Matrix: Gamma(k)
Param = func_Choose_Gamma_init(Param, Latent, Variable);

%% Run Microphone Array Geometry Estimation
%% 1) Estimate Coherence Matrix of Reverberation Using ECM Algorithm
[Param, Eval] = func_ECM(Param, Variable);

%% 2) Estimate Pairwise Distances by Minimizing Model-Based Error
% Model should be either sinc or modified sinc, depending on scenario.
[Eval,Param] = func_Minimize_Error_to_Model(Param, Latent, Eval);

%% 3) Estimate Relative Microphone Geometry Multidimensional Scaling 
Eval = func_ClassicalMDS(Eval, Latent);

%% 4) Evaluation 
% a) Procrustes Analysis
% Align est. relative co-ordinates with true co-ordinates for evaluation
[Eval, Q] = func_Procrustes_Analysis(Latent,Eval);

% b) Find RMS error between estimated and original co-ords
% Compute MAG estimation error
Results = func_Evaluate_Estimation_Error(Param,Latent,Variable,Eval, true);
disp(['Mean calibration error: ' num2str(round(Results.error.mean,3)) ' m.']);
disp(['Median calibration error: ' num2str(round(Results.error.median,3)) ' m.']);

%% Plot PD Error matrix
figure
imagesc(abs(sqrt(Latent.D_sq)-sqrt(Eval.D_sq_est)));
title('Pairwise Distance Errors [m]')
colorbar;
set(gcf, 'Position', get(0, 'Screensize'));