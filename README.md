Description: 

This code is heavily based on a recent iterative approach [1] to estimate the geometry of a distributed microphone array using reverberant speech. 
All steps of the algorithm itself are coded from scratch.

My implementation includes some changes to the initialization of the iterative approach which improves the geometry estimation performance,
outlined in an upcoming publication (ITG 2021), which will be linked here as soon as it is live. 

If you have any questions or ideas for collaboration feel free to get in touch: klaus.bruemann@uol.de

---

Two test scripts are provided:
- Estimate_MAG_HabetsRIR_0_3_m.m
  An anechoic speech signal is convolved with simulated room impulse responses (RIRs) [2].
  The geometry of 6 distributed microphones, randomly located within an 0.3 x 0.3 x 0.3 m cube, is calibrated.
- Estimate_MAG_Artificial_1_5_m.m
  A test signal is generated completely according to the signal model in the short-time Fourier-transform domain for testing purposes.
  The geometry of 6 distributed microphones, randomly located within an 1.5 x 1.5 x 1.5 m cube, is calibrated.

---

Requirements:
- Signal Processing Toolbox 
- Statistics and Machine Learning Toolbox 
- This code likely requires a Matlab version of 2016b or newer due to the way some matrix operations are written with implicit matrix expansion. 
- If the code is being run on an OS other than windows, it may require a re-compilation of the mex function included RIR generator [2] (within the "Auxiliary_Functions" folder). 

---

References:

[1] Schwartz, Ofer, et al. "Blind microphone geometry calibration using one reverberant speech event." 2017 IEEE Workshop on Applications of Signal Processing to Audio and Acoustics (WASPAA). IEEE, 2017.

[2] https://github.com/ehabets/RIR-Generator
