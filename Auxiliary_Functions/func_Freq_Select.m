function [f_vec, select_freqs, f_vec_selected] = func_Freq_Select(K, fs, f_range)
f_vec = ((1:(K/2+1))-1)/K*fs; % center frequencies of FFT (or STFT) bins
freq_inds = 1:(K/2+1);
select_freqs = freq_inds(f_vec >= f_range(1) & f_vec <= f_range(2));
f_vec_selected = f_vec(select_freqs);
end