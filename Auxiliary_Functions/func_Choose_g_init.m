function Param = func_Choose_g_init(Param, Latent, Variable)
% This function determines the relative-direct transfer function (RDTF) to 
% be used in the expectation conditional maximization (ECM) algorithm for
% geometry calibration.

switch Variable.Sim_type
    case 'Artificial' 
        % In the case of a fully artificial scenario/signal, generate oracle RDTF
        switch Variable.RDTF_type
            case 'T=0'
                % Expectation of normalised speech vector over time frames
                Param.g = mean(Latent.s ./ Latent.s(1,1,:,:),4);
            otherwise % i.e.: 'TDoAs'
                % Generative model (based off of TDoAs)
                if ~isfield(Variable,'TDoA_Error')
                    Param.g = Latent.g_Oracle;
                else
                    Param.g = exp( -1i*2*pi* (...
                        Latent.TDoAs_samples + Variable.TDoA_Error ) .* ...
                        permute(0:Param.K/2,[3,1,2])/Param.K );
                end
        end
    case 'HabetsRIR'
        % In the case of a simulated scenario, 
        % choose between oracle or estimated TDoAs:
        switch Variable.RDTF_type
            case 'TDoA' % Use oracle TDoA based on source-mic geometry
                if ~isfield(Variable,'TDoA_Error')
                    Variable.TDoA_Error = 0;
                end
                Param.g = exp( -1i*2*pi* (...
                    Latent.TDoAs_samples + Variable.TDoA_Error ) .* ...
                    permute(0:Param.K/2,[3,1,2])/Param.K );
            case 'GCCPHAT' % Use estimated TDoA:
%                 N = 256;            % Framesize
                lambda = 0.95;      % Averaging factor
                weighting = 1;      % PHAT-weighting
                intfactor = 5;      % Interpolation factor
                
                TDoA_vec = zeros(Param.N,1);
                for ii = 2:Param.N
                    TDoA_vec(ii) = gcc(Param.x_t(:,[1,ii]),Param.K,lambda,weighting,Param.Ana_win, ...
                        Param.R,2*Param.K,intfactor);
                end
                % TDoA_vec = GCC_delays(Param.x_t,Param.N,Param.K,Param.R, ...
                %     Param.L,Param.fs,Param.c,Param.l_max);

                Param.g = exp( -1i*2*pi*TDoA_vec.* ...
                    permute(0:Param.K/2,[3,1,2])/Param.K );
            otherwise % 'Oracle' - obtained in func_Generate_RIR_Habets
                % This RDTF estimate is more of an RTF estimate, using
                % oracle information from the RIR.
                Param.g = Latent.g_Oracle;
        end
end

end
