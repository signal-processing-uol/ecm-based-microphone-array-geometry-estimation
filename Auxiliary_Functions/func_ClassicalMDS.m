%% Classical MDS
% Finds the best fitting geometry upto an arbitrary rigid transformation
% (translation, rotation, or reflection)
%
% Implementation based on:
% 'Euclidean Distance Matrices', Dokmanic et al., 2015

function [Eval] = func_ClassicalMDS(Eval, Latent)
% Number of microphones
n = size(Eval.D_sq_est,1);
% Geometric centering matrix
J = eye(n) - 1/n;
% compute Gram matrix
G_est = -1/2*J*Eval.D_sq_est*J;
% Ensure G is numerically symmetric for E-value decomposition
G_est = 0.5*(G_est + G_est');

% Eigenvalue decomposition of gram matrix (centered EDM)
[U,Lambda_vec] = eig(G_est,'vector');

% Essential fix to avoid negative distances
Lambda_vec = max(Lambda_vec,0);

% Sort eigenvalues and eigenvectors
[~,ind]=sort(abs(Lambda_vec),'descend');
% [~,ind]=sort(real(Lambda_vec),'descend');
U = U(:,ind);

% Truncate eigenvalues to conform to p-dimensional Euclidean vector space
Lambda_mat_sqrt = [diag(sqrt(Lambda_vec(ind(1:min(Latent.p,n),1)))),zeros(Latent.p,n-Latent.p)];

% Reconstruct relative microphone geometry 
% (related to the microphone geometry by an arbitrary translation/rotation/reflection)
Eval.M_est_rel = real(Lambda_mat_sqrt*U');

% Reconstruct low-rank PD matrix based on estimated relative mic. geometry.
% Previously, Eval.D_sq_est is likely rank n due to estimation errors,
% however, here we generate a low-rank approximation corresponding to the
% estimated relative microphone geometry.
onevec = ones(n,1);
Gram_est = U* [Lambda_mat_sqrt;zeros(n-Latent.p,n)].^2 *U';
Eval.D_sq_est_MDS = onevec*diag(Gram_est)' - 2*Gram_est + diag(Gram_est)*onevec';

end