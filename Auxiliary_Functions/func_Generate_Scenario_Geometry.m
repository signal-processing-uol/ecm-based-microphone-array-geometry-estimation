function [Param, Latent] = func_Generate_Scenario_Geometry(Param, Latent)
% This function generates the geometry of the source and microphones in the
% acoustical scenario.

if ~isfield(Latent, 'scenario_rng') % ~exist('scenario_rng','var')
    Latent.scenario_rng = 0;
end

rng(1337+Latent.scenario_rng);

%% Generate microphone positions

% specific geometry generation if N==2 and pairwise distance is given
if Param.N == 2 && isfield(Latent,'mic_dist')
    shifts = rand(3,Param.N)*Latent.mic_dist-Latent.mic_dist/2;
    shifts = [shifts(:,1), -shifts(:,1)];
    shifts = shifts./vecnorm(shifts)/2; % divide by 2 because 2 mics
    Latent.M = zeros(length(Latent.P),Param.N);
    while any(Latent.M <= 0.05*Latent.P | Latent.M >= 0.95*Latent.P, 'all')
        Latent.avg_pos = rand(3,1).*(Latent.P-2*Latent.mic_dist) + Latent.mic_dist;
        Latent.M = Latent.avg_pos + shifts*Latent.mic_dist;
    end
    %% Generate source position
    Latent.q = zeros(length(Latent.P),1); % initialize source position
    ind = 0;
    % if mic is too close to array or to wall
    while any(Latent.q <= 0.05*Latent.P | Latent.q >= 0.95*Latent.P, 'all') ...
            || norm(Latent.q-Latent.avg_pos) < 2*Latent.mic_dist
        Latent.q = rand(3,1).*Latent.P;
        ind = ind + 1;
        if ind > 1000
            error(['Something went wrong with the acoustic scenario generation,'...
                ' please check the parameters.'])
        end
    end
else % Normal geometry generation
    % Generate mic positions within walls
    shifts = rand(3,Param.N)*Latent.l-Latent.l/2;
    Latent.M = zeros(length(Latent.P),Param.N);
    ind = 0;
    while any(Latent.M <= 0.1*Latent.P | Latent.M >= 0.9*Latent.P, 'all')
        Latent.avg_pos = rand(3,1).*(Latent.P-2*0.1*Latent.P) + 0.1*Latent.P;
        Latent.M = Latent.avg_pos + shifts;
        
        ind = ind + 1;
        if ind > 1000
            error(['Something went wrong with the acoustic scenario generation,'...
                ' please check the parameters.'])
        end
    end
    %% Generate source position
    Latent.q = zeros(length(Latent.P),1); % initialize source position
    ind = 0;
    % if mic is too close to array or to wall
    while any(Latent.q <= 0.1*Latent.P | Latent.q >= 0.9*Latent.P, 'all') ...
            || norm(Latent.q-Latent.avg_pos) < 2*Latent.l
        Latent.q = rand(3,1).*Latent.P;
        ind = ind + 1;
        if ind > 1000
            error(['Something went wrong with the acoustic scenario generation,'...
                ' please check the parameters.'])
        end
    end
end


%% Other useful parameters here
Latent.p = rank(Latent.M-Latent.M(:,1)); % rank of array geometry, centered at ref. mic.

Gram = Latent.M'*Latent.M;
onevec = ones(Param.N,1);

% Euclidean Distance Matrix (squared distances):
Latent.D_sq = onevec*diag(Gram)' - 2*Gram + diag(Gram)*onevec';
% can also be computed by computing the pairwise distances then squaring

% Distances proportional to time-differences of arrival (TDoAs):
Latent.deltas = (vecnorm(Latent.q-Latent.M)'-vecnorm(Latent.q-Latent.M(:,1))');
Latent.Delta_Mat = abs(Latent.deltas - Latent.deltas');

% TDoAs:
Latent.TDoAs_seconds = Latent.deltas / Param.c; % in seconds
Latent.TDoAs_samples = Latent.TDoAs_seconds * Param.fs; % in samples

% Just in-case, create folders early on for evaluation
% if ~isfolder('Figures')
%     mkdir('Figures');
% end
% if ~isfolder('Results')
%     mkdir('Results');
% end

% Some extra geometrical info:
Latent.source_to_array_dist_m = sqrt(sum((Latent.q-mean(Latent.M,2)).^2));
Latent.source_to_ref_dist_m = sqrt(sum((Latent.q-Latent.M(:,1)).^2));

end
