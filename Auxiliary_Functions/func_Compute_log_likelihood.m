function f = func_Compute_log_likelihood(Gamma,PSD_S,S_hat,...
    PSD_R,r_hat,L,fv_bins,regularize,loading_factor,eps)

Joint = func_Compute_joint(Gamma,PSD_S,S_hat,...
    PSD_R,r_hat,L,fv_bins,regularize,loading_factor);

% log of product = sum of logs
% summed over time bins
f = mean( sum(Joint,2) );

end