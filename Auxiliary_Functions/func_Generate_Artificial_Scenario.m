function [Param, Latent] = func_Generate_Artificial_Scenario(Param, Latent)

Latent.g_Oracle = exp( -1i*2*pi*Latent.TDoAs_samples .* ...
    permute(0:Param.K/2,[3,1,2])/Param.K );

dir_stddev = db2mag(0); % = 1
rev_stddev = db2mag(Latent.RDR);

S = dir_stddev * sqrt(randn(1,1,Param.K/2+1,Param.L) + ...
    1i*randn(1,1,Param.K/2+1,Param.L));
R = rev_stddev * sqrt(randn(1,1,Param.K/2+1,Param.L) + ...
    1i*randn(1,1,Param.K/2+1,Param.L));

% Generate Gamma based on pairwise distances
Gamma_Oracle = func_Gamma_sinc(Param.fv_full, Param.c, sqrt(Latent.D_sq));

Latent.r = R .* func_Generate_Vec_from_Cov_matrix(Gamma_Oracle,Param.L);

Latent.s = S .* Latent.g_Oracle;

Param.x = Latent.s + Latent.r;

Param.VAD_STFT = true(size(squeeze(Param.x(1,1,:,:))));

end
