function [error] = func_calc_error(X_aligned, MicCoords,plot_on)

% Preallocate error matrix
errorMat = zeros(size(MicCoords,2),1);

% Error for each microphone
RMS_temp = vecnorm(X_aligned - MicCoords);
%     sqrt(sum( (X_aligned.(X_fields{ii}) - MicCoords).^2, 1) );

% Average error for each microphone:
error.mean = mean( RMS_temp );
error.median = median( RMS_temp );
error.vec = RMS_temp(:);
errorMat(:,1) = RMS_temp(:);
% errorMat(:,ii) = RMS_temp(:);

if plot_on
    figure
    boxplot(errorMat)
    title(['Geometry Calibration Error Boxplot (' num2str(size(MicCoords,2)) ' Mics.)'])
    ylabel('Calibration Error [m]')
    xticklabels('')
    temp = ylim;
    ylim([0,temp(2)]);
    set(gcf, 'Position', get(0, 'Screensize'));
end
end