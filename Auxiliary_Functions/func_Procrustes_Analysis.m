%% Procrustes Analysis
% Finds best rigid transformation which maps a set of anchors
% (the number of anchors is defined by num_anchors) of an estimated 
% geometry to a target geometry
%
% Implementation based on:
% 'Euclidean Distance Matrices', Dokmanic et al., 2015

function [Eval, R] = func_Procrustes_Analysis(Latent,Eval)

% convention:
% - 1st dim are co-ordinates x,y,..
% - 2nd dim are mics m = 1,2,3,...

% use all anchors:
num_anchors = size(Latent.M,2); % (number of channels)

[~,n] = size(Latent.M);
% e.g.: num_anchors = n;

% Select a small set of anchors of X to align with Y (>=d)
Anchor_inds = sort(randperm(n,num_anchors)); % sort is not really needed
X_a = Eval.M_est_rel;
X_a(:,~any(Anchor_inds'==(1:n))) = 0;

% Compute means:
y_c = mean(Latent.M,2);
x_ac = mean(X_a,2);

% Remove means
Y_bar = Latent.M - y_c;
X_a_bar = X_a - x_ac;

% Apply singular value decomposition to find optimal transformation R
[U,~,V] = svd(X_a_bar*Y_bar','econ');

% Solution: optimal rigid transformation
R = V*U';

% Return back-transformed point set
Eval.M_est_aligned = R*(Eval.M_est_rel-x_ac) + y_c;

end