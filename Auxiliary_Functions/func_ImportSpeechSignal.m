function [signalnames] = func_ImportSpeechSignal(x)
rng(1337+x)
dirname = '48k';
foldernames = dir(dirname);
foldernames = {foldernames.name};
foldernames = foldernames(3:end-1);
foldernames = foldernames(cellfun(@length,foldernames)==2);
foldernames = foldernames(randi(length(foldernames),1,1));
% signalnames = [foldernames '02_02.wav'];
for ii = 1:length(foldernames)
    temp = dir(fullfile(dirname,foldernames{ii}));
    temp = {temp.name};
    temp = temp(3:end-1);
    temp = temp(randi(length(temp),1,1));
    for jj = 1:length(temp)
        signalnames{ii,jj} = fullfile(dirname, foldernames{ii}, temp{jj}); %#ok<AGROW>
    end
    %     signalnames{ii} = fullfile(dirname, foldernames{ii}, temp); %#ok<SAGROW>
end