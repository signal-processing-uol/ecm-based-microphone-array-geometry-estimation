function [Param, Latent] = func_Generate_RIRs_Habets(Param, Latent, Variable)
%% Habets RIRgen parameters
n = 4096; % 4096;                   % Number of samples
mtype = 'omnidirectional';  % Type of microphone
order = -1;                 % -1 equals maximum reflection order!
dim = length(Latent.P');                    % Room dimension
orientation = 0;            % Microphone orientation (rad)
hp_filter = 1;              % Enable high-pass filter

% Vary T60 until desired DRR is achieved
T60_upper = 1; % high T60 yields low DRR
T60_lower = 0.2; % low T60 yields high DRR
[h_t_upper,~] = rir_generator(...
    Param.c, Param.fs, Latent.M', Latent.q', Latent.P', T60_upper, ...
    n, mtype, order, dim, orientation, hp_filter);
[h_t_lower,~] = rir_generator(...
    Param.c, Param.fs, Latent.M', Latent.q', Latent.P', T60_lower, ...
    n, mtype, order, dim, orientation, hp_filter);
% -RDR = DRR

DRR = -Latent.RDR;

DRR_upper = calc_DRR(h_t_upper(1,:)',Param.fs);
DRR_lower = calc_DRR(h_t_lower(1,:)',Param.fs);
if (DRR < DRR_upper) || ...
        (DRR > DRR_lower)
    disp(['Upper DRR: ' num2str(DRR_upper), ...
        ', T60: ' num2str(T60_upper) ...
        ', lower DRR: ' num2str(DRR_lower), ...
        ', T60: ' num2str(T60_lower)])
    warning('on');
    warning('DRR out of range of scenarios that can be simulated');
    warning('off')
%     error('DRR out of range of scenarios that can be simulated');
end

if (DRR < DRR_upper)
    T60_middle = T60_upper;
    h_t_middle = h_t_upper;
    DRR_middle = DRR_upper;
elseif (DRR > DRR_lower)
    T60_middle = T60_lower;
    h_t_middle = h_t_lower;
    DRR_middle = DRR_lower;
else
    T60_middle = T60_lower + (T60_upper-T60_lower)/2;
    [h_t_middle,~] = rir_generator(...
        Param.c, Param.fs, Latent.M', Latent.q', Latent.P', T60_middle, ...
        n, mtype, order, dim, orientation, hp_filter);
    DRR_middle = calc_DRR(h_t_middle(1,:)',Param.fs);
    counter = 0;
    while abs(DRR-DRR_middle) > 1 % while DRR is not within 1 dB of target
        if (DRR >= DRR_upper && DRR <= DRR_middle)
%             T60_middle = T60_upper;
            T60_lower = T60_middle;
            DRR_lower = DRR_middle;
            %         [h_t_lower,~] = rir_generator(...
            %             Param.c, Param.fs, Latent.M', Latent.q', Latent.P', T60_lower, ...
            %             n, mtype, order, dim, orientation, hp_filter);
            %         DRR_lower = calc_DRR(h_t_lower(1,:)',Param.fs);
        elseif (DRR >= DRR_middle && DRR <= DRR_lower)
%             T60_middle = T60_lower;
            T60_upper = T60_middle;
            DRR_upper = DRR_middle;
            %         [h_t_upper,~] = rir_generator(...
            %             Param.c, Param.fs, Latent.M', Latent.q', Latent.P', T60_upper, ...
            %             n, mtype, order, dim, orientation, hp_filter);
            %         DRR_upper = calc_DRR(h_t_upper(1,:)',Param.fs);
        end
        T60_middle = T60_lower + (T60_upper-T60_lower)/2;
        [h_t_middle,~] = rir_generator(...
            Param.c, Param.fs, Latent.M', Latent.q', Latent.P', T60_middle, ...
            n, mtype, order, dim, orientation, hp_filter);
        DRR_middle = calc_DRR(h_t_middle(1,:)',Param.fs);
        counter = counter + 1;
        if counter > 10 % && ~mod(counter,10)
            disp(['counter: ' num2str(counter)]);
            disp(['T60: ' num2str(T60_middle)]);
            disp([]);
        end
        if counter > 100
            error('Recheck code to calculate rirs with given RDR')
        end
    end
end
disp(['Setting DRR to: ' num2str(DRR_middle)])
Latent.h_t = h_t_middle.';
Latent.RDR_ = -DRR_middle;

% Generate oracle RDTF/RETF
if strcmp(Variable.RDTF_type(1:2),'T=')
    Variable.RIR_cut_time = str2double(Variable.RDTF_type(3:end))*1e-3;
    
end

if strcmp(Variable.RDTF_type(1:2),'T=') || strcmp(Variable.Gamma_Init_type,'Oracle')
    % if the oracle coherence is used, then assume oracle RDTF is known too
    if strcmp(Variable.RDTF_type,'T=0') || ...
            (strcmp(Variable.Gamma_Init_type,'Oracle')&&~strcmp(Variable.RDTF_type(1:2),'T=')) %
        order = 0;
        [Latent.g_t,Latent.beta_hat] = rir_generator(...
            Param.c, Param.fs, Latent.M', Latent.q', Latent.P', T60_middle, ...
            n, mtype, order, dim, orientation, hp_filter);
        Latent.g_t = Latent.g_t.';
        Latent.g_Oracle = permute(...
            rir2rtf(Latent.g_t, Param.fs, Param.K, 1, Param.Ana_window)...
            ,[1,4,2,3]);
    else
        window_len = 128;
        late_window = hann(window_len,'periodic');
        late_window = late_window((window_len/2):end);
        % determine how many samples after peak to cut
        [~, h_t_peak_ind] = max(abs(Latent.h_t));
        sample_delay = h_t_peak_ind + ceil(Variable.RIR_cut_time*Param.fs);
        Latent.g_t = Latent.h_t;
        for ii = 1:Param.N
            Latent.g_t(sample_delay(ii)+(0:(window_len/2)),ii)...
                = Latent.g_t(sample_delay(ii)+(0:(window_len/2)),ii).*late_window;
            Latent.g_t((sample_delay(ii)+(window_len/2+1)):end,ii) = 0;
            %         Latent.g_t(ceil(h_t_peak_ind(ii)+sample_delay):end,ii) = 0;
        end
        Latent.g_Oracle = permute(...
            rir2rtf(Latent.g_t, Param.fs, Param.K, 1, Param.Ana_window)...
            ,[1,4,2,3]);
    end
    
    % Estimate reverberant RIR here
    Latent.rev_t = Latent.h_t - Latent.g_t;
end

% If the TSP database is used (48k folder), simply include the folder in the
% same directory as the geometry calibration script and uncomment the
% following code to randomly select speech files 
% (and comment out the two proceeding lines of code):
% z_t = [];
% counter = 1;
% while size(z_t,1) < (1-Param.R/Param.K)*Param.K + Param.R*(Param.L-1)
%     if ~isfield(Latent, 'scenario_rng') % modify seed for rand sig generation
%         Latent.scenario_rng = 0;
%         Latent.signalnames = func_ImportSpeechSignal(counter);
%     else
%         Latent.signalnames = ...
%             func_ImportSpeechSignal(Latent.scenario_rng + counter-1);
%     end
%     
%     [z_t_temp,fsrec] = audioread(Latent.signalnames{1});
%     
%     z_t = [z_t; resample(z_t_temp,Param.fs,fsrec,100)]; %#ok<AGROW>
%     counter = counter + 1;
% end
[z_t_temp,fsrec] = audioread(fullfile('Audio','clean_yorkshire_typ.wav'));
z_t = resample(z_t_temp(:,1),Param.fs,fsrec,100);

[z_t_r, z_t_rev] = deal(zeros(size(Latent.h_t,1)+size(z_t,1)-1,size(Latent.h_t,2)));
for ii = 1:size(Latent.h_t,2)
    z_t_r(:,ii) = conv(z_t,Latent.h_t(:,ii));
end

Param.x_t = z_t_r(1:min(Param.K + Param.R*(Param.L-1),length(z_t_r)),:); % (1-Param.R/Param.K)*Param.K
Param.x_t = (Param.x_t - mean(Param.x_t,1)) ./ std(Param.x_t,[],1); % normalize signal
x = permute(calc_STFT(Param.x_t,Param.fs,Param.K,Param.K/Param.R, Param.Ana_window),[1,4,2,3]);
Param.L = min(Param.L,size(x,4));
Param.x = x(:,:,:,1:Param.L);

if strcmp(Variable.RDTF_type(1:2),'T=') || strcmp(Variable.Gamma_Init_type,'Oracle')
    for ii = 1:size(Latent.h_t,2)
        z_t_rev(:,ii) = conv(z_t,Latent.rev_t(:,ii));
    end
    Latent.r_t = z_t_rev(1:min(Param.K + Param.R*(Param.L-1),length(z_t_r)),:); % (1-Param.R/Param.K)*Param.K
    Latent.r_t = (Latent.r_t - mean(Latent.r_t,1)) ./ std(Latent.r_t,[],1); % normalize signal
    r = permute(calc_STFT(Latent.r_t,Param.fs,Param.K,Param.K/Param.R, Param.Ana_window),[1,4,2,3]);
    Latent.r = r(:,:,:,1:Param.L);
    Latent.r(isnan(Latent.r) | isinf(Latent.r)) = 0;
    
    z_t_S(:,1) = conv(z_t,Latent.g_t(:,1));
    Latent.S_t = z_t_S(1:min(Param.K + Param.R*(Param.L-1),length(z_t_r)),1); % (1-Param.R/Param.K)*Param.K
    Latent.S_t = (Latent.S_t - mean(Latent.S_t,1)) ./ std(Latent.S_t,[],1); % normalize signal
    S = permute(calc_STFT(Latent.S_t,Param.fs,Param.K,Param.K/Param.R, Param.Ana_window),[1,4,2,3]);
    Latent.S = S(:,:,:,1:Param.L);
    Latent.S(isnan(Latent.r) | isinf(Latent.r)) = 0;
end

% Determine VAD in first mic. 
% Only base on frequencies required for PD estimation
z_t_VAD = resample(z_t_r(:,1),100,Param.fs,100);
z_t_VAD = resample(z_t_VAD,Param.fs,100,100);
Param = vad_opt(z_t_VAD,Param, -20, 5e-3);

% subplot(2,1,1)
% imagesc(db(squeeze(Param.x(1,1,:,:)))); axis xy; colorbar; colormap('jet')
% subplot(2,1,2)
% imagesc(squeeze(Param.VAD_STFT)); axis xy; colorbar; colormap('jet')
% drawnow;
end
