function Gamma = func_Gamma_sinc(f_vec, c, PD)
omega_vec = permute(2*pi*f_vec,[1,3,2]); % cyclical frequency
xx=@(d) d.*omega_vec / c; % 2*pi*f*t
Gamma_sinc=@(d) sinc(xx(d)/pi);
Gamma = Gamma_sinc(PD);
end

