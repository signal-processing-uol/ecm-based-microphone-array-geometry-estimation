function r = func_Generate_Vec_from_Cov_matrix(Cov,L)
rng(420)  % For reproducibility
% mu = [0 0 0];
% Sigma = [1 0.7 0.5; 0.7 1 0.7; 0.5 0.7, 1];
[N,~,K] = size(Cov);
mu = zeros(1,N); % assume zero-mean ovr time 4 all frequencies and channels
r = zeros(N,1,K,L);
for kk = 1:K
    [U,d] = eig(Cov(:,:,kk),'vector');
    if ~all(d >= 0) % make sure PSD
        d(d<0) = 0;
        Cov(:,:,kk) = U*diag(d)*U';
    end
    Cov(:,:,kk) = Cov(:,:,kk) + Cov(:,:,kk)'; % make sure symmetric
    r(:,1,kk,:) = permute(mvnrnd(mu,Cov(:,:,kk),L),[2,3,4,1]);
    % x(:,1,kk,:) = mean(x_temp.*conj(permute(x_temp,[2,1,3,4])),3);
end
end