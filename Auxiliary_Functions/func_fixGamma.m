function [Gamma] = func_fixGamma(Gamma, select_inds, regularize, ...
    loading_factor, N)
%% Regularize via eigenvalues
% [V,U]=eig(0.5*(Gamma + Gamma'),'vector');
% Gamma = V * diag(max(real(U),db2pow(-60))) * V';

Gamma = real(Gamma);

%% Trace normalization
for kk = select_inds
    % divide Gamma by N instead of trace due to numerical reasons
%     Gamma(:,:,kk) = Gamma(:,:,kk) ./ (trace(regularize(Gamma(:,:,kk)/N, loading_factor)));
    Gamma(:,:,kk) = Gamma(:,:,kk) ./ (trace(regularize(Gamma(:,:,kk)/N, loading_factor)));
%     Gamma(:,:,kk) = Gamma(:,:,kk)./sqrt(diag(Gamma(:,:,kk)))./sqrt(diag(Gamma(:,:,kk))');
end

% for kk = 1:size(Gamma,3)
%     Gamma(:,:,kk) = Gamma(:,:,kk)./sqrt(diag(Gamma(:,:,kk)))./sqrt(diag(Gamma(:,:,kk))');
% end

%% Don't iterate Gamma if there are any NANs 
% (leave this out since it unfairly favours some estimates (e.g. oracle))
% select_NaN = any( isnan( Gamma(:,:,select_inds) ), [1,2]);
% Gamma(:,:,select_NaN) = Gamma_old(:,:,select_NaN);

%% Force Hermitian
Gamma = 0.5*(Gamma + conj(permute(Gamma,[2,1,3,4])));

end