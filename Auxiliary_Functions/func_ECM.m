% Expectation Conditional Maximization for estimating:
% > time-invariant coherence matrix (Param.Gamma)
% > time-varying speech PSD (Param.PSD_S)
% > time-varying noise PSD (Param.PSD_R)
%
%% Some useful notes:
% > in the STFT indexing, four dimensions are used (i,j,k,l).
% (i) denotes mic. index, (i,j) used together denote cross-mic. index,
% (k) denotes frequency bin, (l) denotes time frame.

% > Some practical considerations are implemented from Schwartz et al.'s
% TASLP 2016 paper:
% "An Expectation-Maximization Algorithm for Multimicrophone Speech
% Dereverberation and Noise Reduction With Coherence Matrix Estimation"
function [Param, Eval] = func_ECM(Param, Variable)

warning('off')
% use halved spectrum to save computation
Param.K_half = Param.K/2+1; 

% define anonymous regularization function here:
regularize=@(A, loading_factor) ...
    A + eye(Param.N)*(trace(A)*loading_factor/Param.N + Param.eps);

% set default filter to compute initial coherence matrix estimate (proposed)
if ~isfield(Variable,'Init_filter')
    Variable.Init_filter = 'MPDR2';
end

%% Initialization 
% of parameters:
ll_inds = 1:Param.L;
[PSD_R, PSD_S, S_hat, S_pow_hat] = deal(ones(1,1,Param.K_half,Param.L));
[r_hat] = deal(zeros(Param.N,1,Param.K_half,Param.L));
[rr_H_hat] = repmat(eye(Param.N),1,1,Param.K_half,Param.L);

% Preallocation:
% Reverberant Speech Covariance matrix
Phi_x = (Param.x).*permute(conj(Param.x),[2,1,3,4]); %ones(M,M,K_half,L);
Phi_x = 0.5*(Phi_x + permute(conj(Phi_x),[2,1,3,4]));
% Reverberant speech PSDs (correspond to diagonal elements of Phi_x)
x_PSDs = max(abs(Param.x).^2, Param.eps); 
Upper_lim_R(1,1,:,:) = mean(x_PSDs,1);
Upper_lim_S(1,1,:,:) = max(x_PSDs,[],1); %  - min(abs(x).^2,[],1)

% Initialize coherence matrix
Param.Gamma = Param.Gamma_Init;
% Coherence matrix Gamma is already initialized when it is passed as input.
% Track variation over ECM iterations.
Track_Gamma = repmat(Param.Gamma,1,1,1,Param.ECM_max_iter+1);
Track_Condition_num = zeros(length(Param.fv_full),Param.ECM_max_iter+1);
f = zeros(Param.ECM_max_iter+1,1);

Param.Joint = zeros(Param.K_half,Param.L,Param.ECM_max_iter+1);
Param.Joint_per_freq = zeros(Param.K_half,Param.ECM_max_iter+1);
%% Initialize speech and reverberation PSDs with ML solutions
Cov_mat = zeros(size(Phi_x));

% Initialize covariance matrix required for filter computation differently
% based on which filter is used to compute initial coherence matrix
% estimate.
switch Variable.Init_filter 
    case 'MPDR' % time-invariant MPDR filter
        Cov_mat_temp = mean(Phi_x,4);
        for kk = Param.fv_bins % Loop over selected frequency bins
            for ll = ll_inds % Loop over time frames
                Cov_mat(:,:,kk,ll) = real(Cov_mat_temp(:,:,kk));
            end
        end
    case 'MPDR2' % time-varying MPDR filter
        Cov_mat_temp = mean(real(Phi_x),4);

        lambda = 0.9;
        for kk = Param.fv_bins % Loop over selected frequency bins
            for ll = ll_inds % Loop over time frames
                if ll > 1
                    Cov_mat(:,:,kk,ll) = lambda*Cov_mat(:,:,kk,ll-1) + (1-lambda)*Phi_x(:,:,kk,ll);
                else
                    Cov_mat(:,:,kk,ll) = lambda*Cov_mat_temp(:,:,kk) + (1-lambda)*Phi_x(:,:,kk,ll);
                end
            end
        end
    otherwise % 'MVDR' - Time-invariant. 
        for kk = Param.fv_bins % Loop over selected frequency bins
            for ll = ll_inds % Loop over time frames
                Cov_mat(:,:,kk,ll) = Param.Gamma(:,:,kk);
            end
        end
end

for kk = Param.fv_bins % Loop over selected frequency bins
    % Check condition number of initial gamma
    [~,U] = eig(Param.Gamma(:,:,kk),'vector');
    Track_Condition_num(kk,1) = max(U)/min(U);
    
    for ll = ll_inds % Loop over time frames
        % Filter for computation of initial coherence matrix estimate.
        % Compute filter per time-frequency bin because some proposed
        % implementations produce a time-varying initial coherence estimate
        w_num = regularize(Cov_mat(:,:,kk,ll), Param.loading_factor)\Param.g(:,1,kk);
        w = w_num ./ real(Param.g(:,1,kk)'*w_num);
        
        % Mathematically this is the same as the preallocated Phi_x but
        % numerically it differs in the region of 1e-18:
        % Phi_x(:,:,kk,ll) = Param.x(:,1,kk,ll)*(Param.x(:,1,kk,ll)');
        
        % For some reason, applying this "numerical trick" here seems to 
        % cause problems (as opposed to applying it in batch-mode before 
        % the loops):
        % Phi_x(:,:,kk,ll) = 0.5*(Phi_x(:,:,kk,ll) + Phi_x(:,:,kk,ll)');
        
        % Rewritten ML solution for noise PSD (equivalent in theory)
        PSD_R(1,1,kk,ll) = max(    Param.eps, ...
            real(min( 1/(Param.N-1) * trace(regularize(Param.Gamma(:,:,kk), Param.loading_factor) \ ...
            (eye(Param.N)-Param.g(:,1,kk)*w')*Phi_x(:,:,kk,ll)), ...
            Upper_lim_R(1,1,kk,ll)) )    );
        
        % ML solution for speech PSD
        cPSD_S = Phi_x(:,:,kk,ll) - PSD_R(1,1,kk,ll)*Param.Gamma(:,:,kk);
        PSD_S(1,1,kk,ll) = max(    Param.eps, ...
            real(min( w' * 0.5*(cPSD_S + cPSD_S') * w, Upper_lim_S(1,1,kk,ll)) )    );
    end
end

Param.Joint(Param.fv_bins,:,1) = func_Compute_joint(Param.Gamma,PSD_S,S_hat,...
    PSD_R,r_hat,Param.L,Param.fv_bins,regularize,Param.loading_factor,Param.eps);
Param.Joint_per_freq(Param.fv_bins,1) = sum(Param.Joint(Param.fv_bins,ll_inds,1),2);

% log of product = sum of logs summed over time bins
% ignore 1st freq bin
f(1) = mean( Param.Joint_per_freq(Param.fv_bins(Param.fv_bins~=1),1) , 1 );

%% Iterate ECM
for iter = 1:Param.ECM_max_iter % ECM Iteration loop
    % Compute g * g' once before frequency bins loop to save computation
    ggH = Param.g.*permute(conj(Param.g),[2,1,3]);
    
    % Update/re-estimate microphone PSD matrix each iteration
    Phi_x(:,:,Param.fv_bins,:) = ggH(:,:,Param.fv_bins).*PSD_S(:,:,Param.fv_bins,:)+...
        Param.Gamma(:,:,Param.fv_bins).*PSD_R(:,:,Param.fv_bins,:);
    
    for kk = Param.fv_bins % Loop over selected frequency bins
        for ll = ll_inds % Loop over time frames
            %% E-Step -----------------------------------------------------
            % Estimate reverberation vector
            % Phi_x_inv_mult_x = regularize(Phi_x(:,:,kk,ll), Param.loading_factor)\x(:,1,kk,ll);
            Gamma_mult_Phi_x_inv = (Param.Gamma(:,:,kk)/regularize(Phi_x(:,:,kk,ll), Param.loading_factor));
            r_hat(:,1,kk,ll) = PSD_R(1,1,kk,ll)*Gamma_mult_Phi_x_inv*Param.x(:,1,kk,ll);
            
            % Estimate reverberation covariance matrix
            temp_mat = Gamma_mult_Phi_x_inv*Param.Gamma(:,:,kk);
            rr_H_hat(:,:,kk,ll) = (r_hat(:,1,kk,ll).*r_hat(:,1,kk,ll)') ... % rank 1
                + PSD_R(1,1,kk,ll) * Param.Gamma(:,:,kk) ...                      % rank N
                - (PSD_R(1,1,kk,ll)).^2 * (temp_mat + temp_mat')*0.5;       % rank N ?
            
            % Estimate speech at ref. mic.
            g_mult_Phi_x_inv = (Param.g(:,1,kk)' / regularize(Phi_x(:,:,kk,ll), Param.loading_factor));
            S_hat(1,1,kk,ll) = PSD_S(1,1,kk,ll) * g_mult_Phi_x_inv * Param.x(:,1,kk,ll);
            
            % Estimate speech periodogram, i.e., PSD
            S_pow_hat(1,1,kk,ll) = abs(S_hat(1,1,kk,ll)).^2 + ...
                PSD_S(1,1,kk,ll) - (PSD_S(1,1,kk,ll)).^2 * ...
                real(g_mult_Phi_x_inv * Param.g(:,1,kk)); % force real-valued
            %	real(g(:,1,kk)' * (regularize(x(:,1,kk,ll)*x(:,1,kk,ll)', Param.loading_factor) \ g(:,1,kk)));
        end
    end
    
    %% M-Step -----------------------------------------------------
    % Practical consideration Eq. 63 and 64 (Using only relevant frames)
    % DRR_inds = (PSD_S./PSD_R) > 1; % for estimation of direct component
    
    % Only update Gamma using frames where the reverberant-to-direct ratio
    % is larger than 1 (and the reverberation PSD is larger than 0)
    RDR_inds = (PSD_R./PSD_S) > 1 & (PSD_R > Param.eps);% & permute(Param.VAD_STFT,[3,4,1,2]); % for estimation of direct component
    % DRR_inds = (PSD_S./PSD_R) > 1 & (PSD_S > Param.eps);% & permute(Param.VAD_STFT,[3,4,1,2]); % for estimation of reverberant component
    
    %     Upper_lim_R(1,1,:,:) = mean(x_PSDs,1);
    %     Upper_lim_S(1,1,:,:) = max(x_PSDs,[],1); %  - min(abs(x).^2,[],1)
    
    %     subplot(2,1,1)
    %     imagesc(squeeze(db(Param.x(1,1,:,:)))); axis xy; colorbar; colormap('jet')
    %     subplot(2,1,2)
    %     imagesc(squeeze(RDR_inds)); axis xy; colorbar; colormap('jet')
    %     drawnow;
    
    for kk = Param.fv_bins % Loop over selected frequency bins
        for ll = ll_inds % Loop over time frames
            PSD_S(1,1,kk,ll) = max(    Param.eps, ...
                min( S_pow_hat(1,1,kk,ll), ...
                Upper_lim_S(1,1,kk,ll) )    );
        end
        
        % Only if DRR is larger than 1
        if kk==1
            Param.Gamma(:,:,kk) = ones(Param.N,Param.N);
        else
            % ML estimate of the coherence.
            % Calculate based on real-part of reverberation
            % cross-periodogram matrix to reduce the estimation errors
            % introduced by the imaginary numbers.
            
            % Instead of dividing by the old PSD estimate, I choose to
            % divide by the current periodogram estimate.
            Param.Gamma(:,:,kk) = mean(...
                real(rr_H_hat(:,:,kk,RDR_inds(1,1,kk,:))) ./ ...
                max(mean(abs(r_hat(:,1,kk,RDR_inds(1,1,kk,:))).^2, 1), Param.eps)...
                ,4);
            % Due to the normalization which occurs in func_fixGamma(), it
            % doesn't make a difference to the mathin the end, it simply
            % makes the coherence estimate more robust.
        end
    end
    
    % Apply some numerical "fixes" to the estimated coherence matrix
    Param.Gamma = func_fixGamma(Param.Gamma, Param.fv_bins, regularize, ...
        Param.loading_factor, Param.N);
    
    for kk = Param.fv_bins % Loop over selected frequency bins
        % Compute condition number to analyse the numerical robustness of
        % the coherence estimate.
        if isnan(Param.Gamma(:,:,kk))
            Track_Condition_num(kk,iter+1) = Inf;
        else
            [~,U] = eig(Param.Gamma(:,:,kk),'vector');
            Track_Condition_num(kk,iter+1) = max(real(U))/min(real(U));
        end
        
        % Estimate reverberation PSD, conditioned on coherence estimate.
        for ll = ll_inds % Loop over time frames
            % Practical consideration to bound overestimation of rev. PSD
            PSD_R(1,1,kk,ll) = max( Param.eps, ...
                min(...
                1/Param.N*real(trace( rr_H_hat(:,:,kk,ll) / regularize(Param.Gamma(:,:,kk), Param.loading_factor) )), ...
                Upper_lim_R(1,1,kk,ll)...
                )...
                );
        end
    end
    
    % Display message each 5 iterations
    if ~mod(iter,5)
        disp([num2str(iter) '/' num2str(Param.ECM_max_iter)])
    end
    
    % Store each Gamma matrix for each frequency bin at each iteration
    Track_Gamma(:,:,:,iter+1) = Param.Gamma;
    
    %     [warnmsg, msgid] = lastwarn;
    %     if strcmp(msgid,'MATLAB:singularMatrix')
    %         disp(['Warning from scenario: ' num2str(scen_ind)])
    %     end
    
    Param.Joint(Param.fv_bins,:,iter+1) = func_Compute_joint(Param.Gamma,PSD_S,S_hat,...
        PSD_R,r_hat,Param.L,Param.fv_bins,regularize,Param.loading_factor,Param.eps);
    
    Param.Joint_per_freq(Param.fv_bins,iter+1) = sum(Param.Joint(Param.fv_bins,ll_inds,iter+1),2);
    
    % log of product = sum of logs summed over time bins
    % ignore 1st freq bin
    f(iter+1) = mean( ...
        Param.Joint_per_freq(Param.fv_bins(Param.fv_bins~=1), iter+1)...
        , 1 );
end

% Debugging note
if any(isnan(Param.Gamma),'all')
    disp(['Gamma contains ' num2str(sum(any(isnan(Param.Gamma),[1,2]))) ...
        ' singular matrices.'])
end

% Re-scale coherence to undo the numerical scaling
for ii = 1:Param.N
    for jj = 1:Param.N
        if ii == jj
            Param.Gamma(ii,jj,:) = 1; % coherence to self is always 1
            Track_Gamma(ii,jj,:,:) = 1;
        else
            Param.Gamma(ii,jj,:) = Param.Gamma(ii,jj,:)/(1-Param.loading_factor);
            Track_Gamma(ii,jj,:,:) = Track_Gamma(ii,jj,:,:)/(1-Param.loading_factor);
        end
    end
end

Eval.log_likelihood = f;
Eval.Track_Gamma = Track_Gamma;
Eval.Track_Condition_num = Track_Condition_num(Param.fv_bins,:);

end
