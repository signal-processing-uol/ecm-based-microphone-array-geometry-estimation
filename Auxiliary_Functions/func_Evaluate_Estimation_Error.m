function [Results] = func_Evaluate_Estimation_Error(Param, Latent, ...
    Variable, Eval, plot_on)

% if ~isfolder('Figures')
%     mkdir('Figures');
% end
% if ~isfolder('Results')
%     mkdir('Results');
% end
if ~exist('plot_on','var')
    plot_on = false; % false
end

% Calculate microphone estimation error
[error] = func_calc_error(Eval.M_est_aligned, Latent.M, plot_on);

% Assign various variables to the Results struct for saving
Results.RDR = Latent.RDR;
if isfield(Latent,'RDR_')
    Results.RDR_actual = Latent.RDR_;
end
Results.M_est_aligned = Eval.M_est_aligned;
Results.M = Latent.M;

Results.D_sq_est = Eval.D_sq_est;
Results.D_sq = Latent.D_sq;
Results.D_error = abs(sqrt(Eval.D_sq_est)-sqrt(Latent.D_sq));
Results.D_MDS_error = abs(sqrt(Eval.D_sq_est_MDS)-sqrt(Latent.D_sq));

Results.error = error;
Results.sourcepos = Latent.q;
Results.Roomdimensions = Latent.P;
Results.Track_Gamma = Eval.Track_Gamma;
Results.Track_Condition_num = Eval.Track_Condition_num;
Results.log_likelihood = Eval.log_likelihood;

Results.Sim_type = Variable.Sim_type;
Results.Gamma_Init_type = Variable.Gamma_Init_type;
Results.RDTF_type = Variable.RDTF_type;

Results.source_to_array_dist_m = Latent.source_to_array_dist_m;

if plot_on
    FS = 28;
    figure
    randinds = rand(size(Latent.M,2),3); % 3 columns for RGB
    
    hold on;
    grid on;
    for ii = 1:size(Latent.M,2)
        p1 = plot3(Latent.M(1,ii),Latent.M(2,ii),Latent.M(3,ii),'o');
        p1.Color = randinds(ii,:);
        %         p1.MarkerFaceColor = randinds(ii,:);
        
        p2 = plot3(Eval.M_est_aligned(1,ii),Eval.M_est_aligned(2,ii),Eval.M_est_aligned(3,ii),'.');
        p2.Color = randinds(ii,:);
        p2.MarkerSize = 12;
        
        p3 = plot3(...
            [Latent.M(1,ii); Eval.M_est_aligned(1,ii)], ...
            [Latent.M(2,ii); Eval.M_est_aligned(2,ii)],...
            [Latent.M(3,ii); Eval.M_est_aligned(3,ii)],'k--');
        
        p3.Color = randinds(ii,:);
        %             title(FN{kk})
        legend('True Locations','Estimated Locations',...
            'Location','Best',...
            'Interpreter','Latex',...
            'FontSize',FS);
    end
    hold off
    
    view(10,20)%view(3)
    avg_pos = mean(Latent.M,2);
    axis([avg_pos(1)-Param.l_max/2, avg_pos(1)+Param.l_max/2, ...
        avg_pos(2)-Param.l_max/2, avg_pos(2)+Param.l_max/2, ...
        avg_pos(3)-Param.l_max/2, avg_pos(3)+Param.l_max/2])
    xlabel('x / \rm{m}','Interpreter','Latex','FontSize',FS)
    ylabel('y / \rm{m}','Interpreter','Latex','FontSize',FS)
    zlabel('z / \rm{m}','Interpreter','Latex','FontSize',FS)
    set(gcf, 'Position', get(0, 'Screensize'));
    %     set(gcf,'Position',[-1920 0 1920 1080])
    set(gca,'TickLabelInterpreter','Latex')
    set(gca,'fontsize',FS)
    drawnow;
end

if plot_on
    FS = 28;
    figure
    randinds = rand(size(Latent.M,2),3); % 3 columns for RGB
    
    p4 = plot3(Latent.q(1),Latent.q(2),Latent.q(3),'ko');
    p4.MarkerFaceColor = [0,0,0];
    
    hold on;
    grid on;
    for ii = 1:size(Latent.M,2)
        p1 = plot3(Latent.M(1,ii),...
            Latent.M(2,ii),...
            Latent.M(3,ii),'o');
        p1.Color = randinds(ii,:);
        
        p2 = plot3(Eval.M_est_aligned(1,ii),...
            Eval.M_est_aligned(2,ii),...
            Eval.M_est_aligned(3,ii),'.');
        p2.Color = randinds(ii,:);
        p2.MarkerSize = 12;
        
        p3 = plot3(...
            [Latent.M(1,ii); Eval.M_est_aligned(1,ii)], ...
            [Latent.M(2,ii); Eval.M_est_aligned(2,ii)],...
            [Latent.M(3,ii); Eval.M_est_aligned(3,ii)],'k--');
        
        p3.Color = randinds(ii,:);
        
        legend('Source Location','True Locations','Estimated Locations',...
            'Location','Best',...
            'Interpreter','Latex',...
            'FontSize',FS);
    end
    
    hold off
    
    view(10,20)%view(3)
    %     avg_pos = mean(Latent.M,2);
    axis([0, Latent.P(1), ...
        0, Latent.P(2), ...
        0, Latent.P(3)])
    xlabel('x / \rm{m}','Interpreter','Latex','FontSize',FS)
    ylabel('y / \rm{m}','Interpreter','Latex','FontSize',FS)
    zlabel('z / \rm{m}','Interpreter','Latex','FontSize',FS)
    set(gcf, 'Position', get(0, 'Screensize'));
    %     set(gcf,'Position',[-1920 0 1920 1080])
    set(gca,'TickLabelInterpreter','Latex')
    set(gca,'fontsize',FS)
    drawnow;
end

end