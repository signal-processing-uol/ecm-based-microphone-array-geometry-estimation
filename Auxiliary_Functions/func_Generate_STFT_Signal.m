function [Param, Latent, Variable] = func_Generate_STFT_Signal(Param, Latent, Variable) 
% Function to generate an STFT signal:
% To generate a signal which is based on the assumed signal model 
% (artificially), set:
%     Variable.Sim_type = 'Artificial'
% alternatively, generate a signal based on a simulated scenario:
%     Variable.Sim_type = 'HabetsRIR'

rng(1337+Latent.scenario_rng);

% Define vectors containing information related to the frequency range
% (e.g., center frequencies, frequency bins)
[Param.fv_full, Param.fv_bins, Param.fv] = ...
    func_Freq_Select(Param.K, Param.fs, Param.fr);

% Assign Analysis windows for STFT:
if strcmpi(Param.Ana_window,'sqrtHann')
    Param.Ana_win  = sqrt(hann(Param.K, 'periodic'));
elseif strcmpi(Param.Ana_window,'Hann')
    Param.Ana_win  = hann(Param.K, 'periodic');
elseif strcmpi(Param.Ana_window,'Hamming')
    Param.Ana_win  = hamming(Param.K, 'periodic');
else
    error(['Unsupported window type "', Param.Ana_window,'"'])
end

% Generate STFT signal and other parameters for geometry estimation 
% (e.g., RDTF or other oracle parameters, if desired):
switch Variable.Sim_type
    case 'Artificial'
        [Param, Latent] = func_Generate_Artificial_Scenario(Param, Latent);
        Variable.Scenario_type = 'FreeField';
    case 'HabetsRIR'
        [Param, Latent] = func_Generate_RIRs_Habets(Param, Latent, Variable);
        Variable.Scenario_type = 'FreeField';
end

end