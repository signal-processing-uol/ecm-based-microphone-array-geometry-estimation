function [Eval,Param] = func_Minimize_Error_to_Model(Param, Latent, Eval, Compute_Coherence_error)
% This function plots

if ~exist('Compute_Coherence_error','var')
    Compute_Coherence_error = false;
end

% find NaNs (from singular Gammas)
Param.fv_bins_modi = Param.fv_bins;
Param.fv_bins_modi(any( isnan( Param.Gamma(:,:,Param.fv_bins_modi) ), [1,2])) = [];

d_vec = 0:0.0001:Param.l_max;

% use selected freq bins from full vector which don't contain NaN
omega_vec = permute(2*pi*Param.fv_full(Param.fv_bins_modi),[1,3,2]); % cyclical frequency
xx=@(d) d.*omega_vec / Param.c; % 2*pi*f*t
Model_Coherence = sinc(xx(d_vec)/pi);

omega_vec_full = permute(2*pi*Param.fv_full(Param.fv_bins),[1,3,2]); % cyclical frequency
xx_full=@(d) d.*omega_vec_full / Param.c; % 2*pi*f*t
Model_Coherence_full_full = sinc(xx_full(d_vec)/pi);


Model_Coherence_full = zeros(Param.N,Param.N,length(Param.fv_bins));
if Compute_Coherence_error
    for ii = 1:Param.N
        for jj = 1:Param.N
            if ii > jj
                [~,ind] = min(abs(sqrt(Latent.D_sq(ii,jj))-d_vec).^2);
                [Model_Coherence_full(ii,jj,:),...
                    Model_Coherence_full(jj,ii,:)] = ...
                    deal(Model_Coherence_full_full(1,ind,:));
            end
        end
    end
end

[Eval.D_est, indsSinc, Eval.Min_Coherence_Error] = deal(zeros(Param.N,Param.N));
[Cost_func_pre_sum, Cost_func_post_sum] = deal(cell(Param.N,Param.N));
for ii = 1:Param.N
    for jj = 1:Param.N
        if ii > jj
            Cost_func_pre_sum{ii,jj} = ...
                abs(Param.Gamma(ii,jj,Param.fv_bins_modi) - Model_Coherence).^2;
            
            % Estimated PD is the one which corresponds to the model
            % coherence which is closest to the estimated coherence
            Cost_func_post_sum{ii,jj} = 1/size(Cost_func_pre_sum{ii,jj},3)* ...
                sum( Cost_func_pre_sum{ii,jj} ,3);
            [Eval.Min_Coherence_Error(ii,jj),indsSinc(ii,jj)] = min(Cost_func_post_sum{ii,jj});
            
            % Apply to opposite elements of symmetric matrix (i,j) -> (j,i)
            [Eval.D_est(ii,jj),Eval.D_est(jj,ii)] = deal(d_vec(indsSinc(ii,jj)));
            
            % Debug:
            %             plot(squeeze(Param.Gamma(ii,jj,Param.fv_bins_modi)))
            %             hold on
            %             plot(squeeze(Model_Coherence(1,indsSinc,:)))
            %             hold off
            %             pause(0.05)
            
            % Compute Coherence error
            if Compute_Coherence_error
                for iii = 1:size(Eval.Track_Gamma,4)
                    [Eval.Coherence_Error(ii,jj,:,iii),...
                        Eval.Coherence_Error(jj,ii,:,iii)] = ...
                        deal(Eval.Track_Gamma(ii,jj,Param.fv_bins,iii) - ...
                        Model_Coherence_full(ii,jj,:));
                end
            end
        end
    end
end

% For geometry estimation using multidimensional scaling we want squared
% pairwise distance matrix, also diagonals should be equal to 0:
Eval.D_sq_est = (Eval.D_est).^2;

%% Example plot code:
Plot_on = true; %true; false;
if Plot_on
    figure
    plotdata = cell(Param.N,Param.N);
    for ii = 1:Param.N
        for jj = 1:Param.N
            subplot(Param.N,Param.N,jj+(ii-1)*Param.N)
            plotdata{ii,jj} = mean(abs(Param.Gamma(ii,jj,Param.fv_bins_modi) - Model_Coherence).^2,3);
            plot(d_vec,plotdata{ii,jj},'b','Linewidth',3)
            hold on
            % h=plot(PD.perfect(ii,jj),0,'ko');
            h=plot(norm(Latent.M(:,ii)-Latent.M(:,jj)),0,'ko');
            h1=plot(Eval.D_est(ii,jj),0,'b.');
            h1.MarkerSize = 10;
            
            hold off
            min_store(ii,jj) = min(plotdata{ii,jj});
        end
    end
    for ii = 1:Param.N
        for jj = 1:Param.N
            subplot(Param.N,Param.N,jj+(ii-1)*Param.N)
            axis([0,d_vec(end), 0, 2*max(min_store(:))])
            grid on
            if ii < Param.N
                xticklabels([]);
                % else
                %    xlabel('$d_{i,j}$ / m','Interpreter','Latex','Fontsize',20)
            end
            xlabel(['$d_{' num2str(ii) ',' num2str(jj) '}$ / m'],...
                'Interpreter','Latex','Fontsize',20)
            if jj > 1
                yticklabels([]);
            else
                ylabel('Error','Interpreter','Latex','Fontsize',20)
            end
            set(gca,'Fontsize',20);
            set(gca,'TickLabelInterpreter','Latex')
            if ii==Param.N && jj==Param.N
                legend('cost func.','True PD','est. PD','Interpreter','Latex')
            end
        end
    end
    sgtitle('Cost Funcs. for Different Pairwise Distances (PDs)',...
        'Interpreter','Latex')
    set(gcf, 'Position', get(0, 'Screensize'));
    %     set(gcf,'Position',[0 50 1080 650])
end

Eval.Cost_func_post_sum = Cost_func_post_sum;
end