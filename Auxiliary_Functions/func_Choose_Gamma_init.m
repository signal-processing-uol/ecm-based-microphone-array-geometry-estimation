function Param = func_Choose_Gamma_init(Param, Latent, Variable)
% This function selects different initializations for the coherence matrix
% of the reverberation, used in the expectation conditional maximization
% (ECM) algorithm for geometry calibration.

switch Variable.Gamma_Init_type
    % These coherence initializations remain the same
    case 'Identity' % Identity matrix initialization which corresponds to infinitely far micrrophones
        Param.Gamma_Init = repmat(eye(Param.N),1,1,length(Param.fv_full));
    case 'Phi_x' % Initialization based on the reverberant speech coherence matrix
        Phi_x = mean(Param.x.*conj(permute(Param.x,[2,1,3,4])),4);
        Param.Gamma_Init = Phi_x; %repmat(Phi_x,1,1,length(Param.fv_full));
    case 'sinc' % Oracle initialization based on sinc-coherence
        Param.Gamma_Init = func_Gamma_sinc( ...
            Param.fv_full, Param.c, sqrt(Latent.D_sq));
    otherwise % signal convolved with reverberant part of RIR
        %         eps = db2pow(-60);
        loading_factor = 0.05;
        Param.Gamma_Init = mean(...
            real((Latent.r .* permute(conj(Latent.r),[2,1,3,4])) ./ ...
            ((sum((abs(Latent.r).^2)*(1+loading_factor)+Param.eps,1))/Param.N))...
            ,4);
end

% Force coherence at f = 0 Hz to 1.
Param.Gamma_Init(:,:,1) = ones(Param.N);

%% Trace-normalize Gamma and other minor practical "tricks"
% Param.eps = db2pow(-60);
% Param.loading_factor = 0.05;
regularize=@(A, loading_factor) ...
    A + eye(Param.N)*(trace(A)*loading_factor/Param.N + Param.eps);
[Param.Gamma_Init] = func_fixGamma(Param.Gamma_Init, ...
    1:size(Param.Gamma_Init,3), regularize, ...
    Param.loading_factor, Param.N);

% Force Hermitian
Param.Gamma_Init = 0.5*(Param.Gamma_Init + ...
    conj(permute(Param.Gamma_Init,[2,1,3])));

end
